export default {
  // Uncomment to enable dark theme
  dark: false,
  options: {
    customProperties: true
  },
  themes: {
    light: {
      primary: "#304a9a",
      secondary: "#13a569",
      backSpiner: "#c4c4c4",
      accent: "#DDB62D",
      error: "#C1272D",
      info: "#98A5CD",
      success: "#13a569",
      warning: "#FFA000"
    },
    dark: {
      primary: "#304a9a",
      secondary: "#13a569",
      backSpiner: "#c4c4c4",
      accent: "#DDB62D",
      error: "#C1272D",
      info: "#98A5CD",
      success: "#13a569",
      warning: "#FFA000"
    }
  }
}  