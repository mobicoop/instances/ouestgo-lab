## This makefile is simply shortcurts for ouestgo docker 


blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)OuestGo ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)OuestGo ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)OuestGo ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info OuestGo ($(os)): Starting ouestgo-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info OuestGo ($(os)): Stopping ouestgo-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep ouestgo_platform
	@docker ps -a | grep ouestgo_db
 
restart:
	$(info OuestGo ($(os)): Restarting ouestgo-platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting Mobicoop-platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info OuestGo ($(os)): Stopping ouestgo-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor
	sudo rm package-lock.json

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)Ouestgo+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f ouestgo_platform

go-platform:
	@docker exec -it ouestgo_platform zsh

go-db:
	@docker exec -it ouestgo_db bash