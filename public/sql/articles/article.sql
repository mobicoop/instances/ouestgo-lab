UPDATE `article` SET `title` = 'Charte et conditions générales d\'utilisation' WHERE `article`.`id` = 1;
UPDATE `article` SET `title` = 'Assurance et covoiturage' WHERE `article`.`id` = 5;
UPDATE `article` SET `title` = 'A l\’origine du projet ouestgo' WHERE `article`.`id` = 6;
UPDATE `article` SET `title` = 'Les acteurs du projet ouestgo' WHERE `article`.`id` = 7;
UPDATE `article` SET `title` = 'Covoiturage solidaire' WHERE `article`.`id` = 8;
UPDATE `article` SET `title` = 'Devenir partenaire' WHERE `article`.`id` = 9;
UPDATE `article` SET `title` = 'Questions fréquentes' WHERE `article`.`id` = 10;
UPDATE `article` SET `title` = 'Boîte à outils' WHERE `article`.`id` = 11;
UPDATE `article` SET `title` = 'COVID-19' WHERE `article`.`id` = 17;