﻿OuestGoLab
=======

![logo](https://www.ouestgo.fr/images/OuestGo/header/logo_gif.gif)


OuestGoLab is based on [mobicoop-plateform](https://gitlab.com/mobicoop/mobicoop-platform)

_for developpement you have to use the same branch of mobicoop for eg (dev mobicoop with dev ouestgolab, master mobicoop=> master ouestgolab)_


# 🐳 With Docker 🐳 


## 🐳 Requirements


Please check mobicoop [docker requirement](https://gitlab.com/mobicoop/mobicoop-platform/tree/dev/docs#-requirements-) 

Clone & install via [docker way](https://gitlab.com/mobicoop/mobicoop-platform/tree/dev/docs#-install) the mobicoop platform

⚡️ Export env variable inside your .zshrc/.bashrc : `export MOBICOOP_CLIENT=/Full/Path/TO/mobicoopl-platform/client`  *(client folder not the bundle!)*

## 🐳 Install 

`make install`


## 🐳 Start 

1. Start Mobicoop
2. Execute mobicoop fixtures
3. `make start`

you can now access ouestgo-lab from `localhost:9091`


# 🐒 Without Docker🐒 

## Install 🐒


1. [Install mobicoop](https://mobicoop.gitlab.io/mobicoop-platform/#/?id=install-🤖)

2. `git clone https://gitlab.com/mobicoop/ouestgo-lab`

3. Link to the mobicoop bundle (from the mobicoop folder): `cd mobicoop-platform && npm run link-bundle ../ouestgo-lab`

4. Back to the ouestgo-lab folder; Install ouestgo-labs dependencies `npm install`


## Start 🐒


0. From the Mobicoop folder, start the api *(& do not quit it)* `cd api && npm start`; api is now running on [localhost:8080](http://localhost:8080)

1. From the OuestGo folder `npm start`, ouestgo-lab is now running on [localhost:8081](http://localhost:8081)

2. If you start all the mobicoop-platform with the front-end, 8081 port will already be used so please choose another one: `npm start 8084`


## Developpement🐒


☣️ If you have to exclude files on mobicoop while developping, *DO NOT FORGET TO EXCLUDE THEM INTO THE [gitlab-exclude](./gitlab-exclude) FILE OF OUESTGO-LAB* ☣️

*If you change any file into the ouestgo-lab mobicoop bundle, files are automatically changed in the mobicoop-platform folder too,  so you will have to commit from the mobicoop-platform folder those changes*


## Content

To populate the dataBase with ouestGo articles. You can find in the sql folder, the files for the articles. Execute sql file in that order article.sql, section.sql and paragraph.sql.