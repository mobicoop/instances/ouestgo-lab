'use strict'
// No overcharge, only import app.js from MobicoopBundle
// import '@js/app.js';

// Else
// Import base css
import '@css/main.scss';
// Import custom css
import '@clientCss/main.scss';

import * as Sentry from "@sentry/vue";

// If you want to overcharge, overcharge the app.js file that loads vue js, you need to reimport everything
// You can overcharge vuetify, i18n etc, juste create the files on the instance and import them in place of config/app.js ones from bundle
import 'babel-polyfill';
import { Vue, vuetify, i18n } from '@js/config/app'

import {merge} from 'lodash'
import bundleComponents from '@js/config/components'
import clientComponents from '@clientJs/config/components'
import { store } from '@js/store'

let components = merge(bundleComponents, clientComponents);

let env = 'production';
if (window.location.href.indexOf(".test.") >= 0){
  env = 'test';
}
if (window.location.href.indexOf("localhost") >= 0){
  env = 'dev';
}

Sentry.init({
  Vue,
  dsn: "https://757990e3434043f8895ffa6f320a838f@sentry.mobicoop.io/3",
  initialScope: {
    tags: { "instance": "ouestgo" },
  },
  integrations: [
    new Sentry.BrowserTracing({
      // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
      tracePropagationTargets: [],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
  environment: env,
});

new Vue({
  el: '#app',
  vuetify,
  i18n,
  store,
  components: components
})