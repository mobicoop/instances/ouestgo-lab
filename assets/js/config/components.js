// BASE
import MHeader from '@clientComponents/base/MHeader'
import MFooter from '@clientComponents/base/MFooter'
import HomeContent from '@clientComponents/home/HomeContent'
import Home from '@clientComponents/home/Home'
import ToolBox from '@clientComponents/utilities/ToolBox/ToolBox'
import MDialog from '@clientComponents/utilities/MDialog'
import Search from '../components/carpool/search/Search.vue'



export default {
  MHeader,
  MFooter,
  HomeContent,
  Home,
  ToolBox,
  MDialog,
  Search
}